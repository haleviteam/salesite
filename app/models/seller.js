var mongoose = require('mongoose');
module.exports = mongoose.model('Seller', {
        name: String,
        active: Boolean,
        email: String,
        password: String
    }, 'sellers'
);