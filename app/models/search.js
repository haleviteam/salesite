var mongoose = require('mongoose');
module.exports = mongoose.model('Search', {
        active: Boolean,
        type: {type: String, enum: ['pishpeshuk', 'yad2']},
        name: String,
        mail: String,
        needed_strings_lists: [{
            any_of: []
        }],
        bad_strings_list: [String],
        max_length: Number,
        posts: [{
            relevant: Boolean,
            last_updated: Date,
            message: String,
            link: String,
            price: String,
            location: [String],
            _id: Number
        }],
        yad2_details: {
            locations: {},
            urls: [String]
        },
        pishpeshuk_details: {
            group_ids: [String]
        }
    }, 'searches'
);