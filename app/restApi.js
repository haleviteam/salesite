var getAll = function (model, req, res) {
    model.find(function (err, items) {
        if (err)
            res.send(err);
        res.json(items);
    });
};
var getOne = function (model, id_param_name, req, res) {
    model.findOne({'_id': req.params[id_param_name]}, function (err, itemFromDb) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.json(itemFromDb);
        }
    });
};
var createNew = function (model, req, res) {
    var newItem = req.body;
    if (newItem._id) {
        model.update({'_id': newItem._id}, newItem, {}, function (err) {
            if (err) {
                res.sendStatus(500);
            }
            else {
                res.sendStatus(200);
            }
        });
    }
    else {
        new model(newItem).save(function (err) {
            if (err) {
                console.log(err);
                res.sendStatus(500);
            }
            else {
                res.sendStatus(200);
            }
        });
    }
};
var deleteOne = function (model, id_param_name, req, res) {
    model.remove({'_id': req.params[id_param_name]}, function (err, itemFromDb) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.sendStatus(200);
        }
    });
};
exports.getAll = getAll;
exports.getOne = getOne;
exports.createNew = createNew;
exports.deleteOne = deleteOne;
