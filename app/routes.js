var Search = require('./models/search');
var Seller = require('./models/seller');
var restApi = require('./restApi');
var fs = require('fs');
var path = require('path');

module.exports = function (app) {
    app.get('/api/searches', function (req, res) {
        restApi.getAll(Search, req, res);
    });
    app.post('/api/searches', function (req, res) {
        restApi.createNew(Search, req, res);
    });
    app.get('/api/search/:searchId', function (req, res) {
        restApi.getOne(Search, 'searchId', req, res);
    });
    app.delete('/api/search/:searchId', function (req, res) {
        restApi.deleteOne(Search, 'searchId', req, res);
    });
    app.delete('/api/post_of_search/:searchId/:postLink', function (req, res) {
        Search.findOneAndUpdate(
            {_id: req.params.searchId},
            {$pull: {posts: {link: req.params.postLink}}},
            function (err, searchFromDb) {
                if (err) {
                    res.status(500).send(err);
                }
                else {
                    res.sendStatus(200);
                }
            });
    });
    app.get('/api/sellers', function (req, res) {
        restApi.getAll(Seller, req, res);
    });
    app.post('/api/sellers', function (req, res) {
        restApi.createNew(Seller, req, res);
    });
    app.get('/api/seller/:sellerId', function (req, res) {
        restApi.getOne(Seller, 'sellerId', req, res);
    });
    app.delete('/api/seller/:sellerId', function (req, res) {
        restApi.deleteOne(Seller, 'sellerId', req, res);
    });
    app.get('*', function (req, res) {
        res.sendFile(path.join(__dirname + '/../public/views/index.html'));
    });

};