angular.module('AllSearchesCtrl', []).controller('AllSearchesController', function ($scope, SearchService) {
    SearchService.all().then(function(response) {
        $scope.rowCollection = response.data;
    });
});