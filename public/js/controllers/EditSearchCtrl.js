angular.module('EditSearchCtrl', []).controller('EditSearchController',
    function ($scope, $location, $routeParams, SearchService, consts) {
        $scope.yad2Name = consts.yad2Name;
        $scope.pishpeshukName = consts.pishpeshukName;
        $scope.searchTypes = [consts.yad2Name, consts.pishpeshukName];

        $scope.addNeededList = function () {
            $scope.search.needed_strings_lists.push({any_of: ['', '']});
        };
        $scope.addNeededStringInList = function (list) {
            list.any_of.push('');
        };
        $scope.addBadString = function () {
            $scope.search.bad_strings_list.push('');
        };
        $scope.addUrl = function () {
            $scope.search.yad2_details.urls.push('');
        };
        $scope.addGroupID = function () {
            $scope.search.pishpeshuk_details.group_ids.push('');
        };

        $scope.getCopyOfDefaultLocations = function () {
            var newObj = {};
            for (var location in consts.locations) {
                newObj[location] = consts.locations[location];
            }
            return newObj;
        };
        $scope.checkAllLocations = function () {
            for (var location in $scope.search.yad2_details.locations) {
                $scope.search.yad2_details.locations[location] = true;
            }
        };
        $scope.uncheckAllLocations = function () {
            for (var location in $scope.search.yad2_details.locations) {
                $scope.search.yad2_details.locations[location] = false;
            }
        };
        $scope.checkDefaultLocations = function () {
            $scope.search.yad2_details.locations = $scope.getCopyOfDefaultLocations();
        };


        $scope.submitted = false;
        $scope.submitForm = function () {
            $scope.submitted = true;
            SearchService.create($scope.search, function () {
                $location.url('/searches');
            });
        };
        $scope.deleteSearch = function () {
            SearchService.delete($scope.search._id, function () {
                $location.url('/searches');
            });
        };
        $scope.deletePost = function (post) {
            console.log(post);
            SearchService.deletePost($scope.search._id, encodeURIComponent(post.link),
                function (response) {
                    console.log(1);
                    $scope.search.posts.splice($scope.search.posts.indexOf(post), 1);
                },
                function (response) {
                    console.log(2);
                })
        };
        if ($routeParams.searchId) {
            $scope.newSearch = false;
            SearchService.get($routeParams.searchId).then(function successCallback(response) {
                $scope.search = response.data;
            }, function errorCallback(response) {
                console.log(response);
            });
        } else {
            $scope.newSearch = true;
            $scope.search = {
                active: true,
                type: '',
                name: '',
                mail: '',
                needed_strings_lists: [
                    {any_of: ['', '']},
                    {any_of: ['', '']}
                ],
                bad_strings_list: ['', ''],
                max_length: '',
                yad2_details: {
                    locations: $scope.getCopyOfDefaultLocations(),
                    urls: ['', '']
                },
                pishpeshuk_details: {
                    group_ids: ['', '']
                }
            };
        }
    })
;
