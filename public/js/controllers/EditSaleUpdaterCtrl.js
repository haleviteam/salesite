angular.module('EditSaleUpdaterCtrl', []).controller('EditSaleUpdaterController',
    function ($scope, $location, $routeParams, SaleUpdaterService) {
        $scope.submitted = false;
        $scope.submitForm = function () {
            $scope.submitted = true;
            SaleUpdaterService.create($scope.seller, function () {
                $location.url('/sale_updaters');
            });
        };
        $scope.deleteSeller = function () {
            SaleUpdaterService.delete($scope.seller._id, function () {
                $location.url('/sale_updaters');
            });
        };
        if ($routeParams.sellerId) {
            $scope.newSeller = false;
            SaleUpdaterService.get($routeParams.sellerId).then(function successCallback(response) {
                $scope.seller = response.data;
            }, function errorCallback(response) {
                console.log(response);
            });
        } else {
            $scope.newSeller = true;
            $scope.seller = {
                active: true,
                name: '',
                email: '',
                password: ''
            };
        }
    })
;
