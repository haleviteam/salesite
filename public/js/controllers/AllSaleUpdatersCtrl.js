angular.module('AllSaleUpdatersCtrl', []).controller('AllSaleUpdatersController', function ($scope, SaleUpdaterService) {
    SaleUpdaterService.all().then(function (response) {
        $scope.rowCollection = response.data;
    });
});