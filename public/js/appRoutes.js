angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })
        .when('/searches', {
            templateUrl: 'views/allSearches.html',
            controller: 'AllSearchesController'
        })
        .when('/search/create/:searchId', {
            templateUrl: 'views/editSearch.html',
            controller: 'EditSearchController'
        })
        .when('/search/create', {
            templateUrl: 'views/editSearch.html',
            controller: 'EditSearchController'
        })
        .when('/sale_updaters', {
            templateUrl: 'views/allSaleUpdaters.html',
            controller: 'AllSaleUpdatersController'
        })
        .when('/sale_updater/create/:sellerId', {
            templateUrl: 'views/editSaleUpdater.html',
            controller: 'EditSaleUpdaterController'
        })
        .when('/sale_updater/create', {
            templateUrl: 'views/editSaleUpdater.html',
            controller: 'EditSaleUpdaterController'
        })
        .otherwise({
        redirectTo: '/home'
    });
    $locationProvider.html5Mode(true);
}]);