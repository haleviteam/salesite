angular.module('SaleUpdaterServiceModule', []).factory('SaleUpdaterService', ['$http', function ($http) {
    return {
        all: function () {
            return $http.get('/api/sellers');
        },
        get: function (searchId, callback) {
            return $http.get('/api/seller/' + searchId).then(callback);
        },
        create: function (searchData, callback) {
            return $http.post('/api/sellers', searchData).then(callback);
        },
        delete: function (sellerId, callback) {
            return $http.delete('/api/seller/' + sellerId).then(callback);
        }
    }
}]);
