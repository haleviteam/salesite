angular.module('SearchServiceModule', []).factory('SearchService', ['$http', function ($http) {
    return {
        all: function () {
            return $http.get('/api/searches');
        },
        get: function (searchId, callback) {
            return $http.get('/api/search/' + searchId).then(callback);
        },
        create: function (searchData, callback) {
            return $http.post('/api/searches', searchData).then(callback);
        },
        delete: function (searchId, callback) {
            return $http.delete('/api/search/' + searchId).then(callback);
        },
        deletePost: function (searchId, postLink, goodCallback, errCallback) {
            console.log('/api/post_of_search/' + searchId + '/' + postLink);
            return $http.delete('/api/post_of_search/' + searchId + '/' + postLink).then(goodCallback, errCallback);
        }
    }
}]);
