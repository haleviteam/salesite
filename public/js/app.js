angular.module('sampleApp',
    [
        'ngRoute', 'appRoutes', 'smart-table',
        'ConstsModule',
        'MainCtrl',
        'AllSearchesCtrl', 'EditSearchCtrl', 'SearchServiceModule',
        'AllSaleUpdatersCtrl', 'EditSaleUpdaterCtrl', 'SaleUpdaterServiceModule'
    ]
);